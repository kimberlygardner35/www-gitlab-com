---
layout: handbook-page-toc
title: "Marketing Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Marketing Finance Handbook!

## Common Links
 * [Financial Planning & Analysis (FP&A)](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/)
 * [Accounting](https://about.gitlab.com/handbook/finance/accounting/)
 * [Allocadia (Marketing Financial Planning tool)](https://about.gitlab.com/handbook/marketing/strategy-performance/allocadia/)


## Finance Business Partner Alignment

| Name | Function |
| -------- | ---- |
| @cmachado1 | Sr. Manager |
| @cmcclure1 | Financial Analyst |


## Marketing Variance Packages

The Marketing Variance Review meeting happens once a month to review the actual expenses for the previous month. For example, in the 3rd week of June we will review May's expenses.  

[Feb'21 Deck](https://docs.google.com/presentation/d/1AfJf45dLs4ch9A8snZhDse1SdNQmKGK0dAkBjmtHAcQ/edit)

[Mar'21 Deck](https://docs.google.com/presentation/d/1cDFGtfyTtyZX8EssJPhtkmMqeEocPYFojYd2TfTOOZY/edit)

[Apr'21 Deck](https://docs.google.com/presentation/d/1H88epd0QWG3IvU1so9sJqFDzffDMAqWaNzWDePQi_ao/edit)

[May'21 Deck](https://docs.google.com/presentation/d/1mBD9u0CQ5-BpO1L4HwV1I2BT08IaPD2f9cTup8B8nfk/edit#slide=id.g1952d3a79ac75c6_215)

## Important Dates
* June Forecast Due - 6/24
* July Forecast Due - 7/23
* Q3 Plan Due - 7/30 
* August Forecast Due - 8/25
* September Forecast Due - 9/24
* October Forecast Due - 10/22
* Q4 Plan Due - 10/29
* November Forecast Due - 11/24
* December Forecast Due - 12/24
* January Forecast Due - 1/25 


## Marketing Budget Holders

The marketing budget holder should be updating their forecast throughout the month in Allocadia as expenses occur and as they have more insight into the spend. On the Forecast Due dates listed above, Finance will take what is in Allocadia and compare that against Actuals in the AvB file. When discrepancies greater than $1000 occur between forecast and actuals, accounting and finance will check with the budget holder to see if an accrual needs to occur.

At the plan due dates above, finance will take what is loaded into Allocadia for the quarter and add that into Adaptive. This is considered your plan and you will be held accountable to plan for the remainder of the quarter. Large variances from the plan will be explained at the monthly variance review meetings with CMO staff. If the department is under-spend, the difference may be reallocated to other departments. 


